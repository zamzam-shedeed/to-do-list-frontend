import App from './App'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import * as actions from '../redux/action'

function mapStateToProps(state){
  return {
    tasks: state.tasks
  }
}

function mapDispatchToProps(dispatch){
  return bindActionCreators(actions ,dispatch)
}

const Main = connect(mapStateToProps,mapDispatchToProps)(App);

export default Main
