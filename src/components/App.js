import React , {Component} from 'react';
import TaskList from './tasksList'
import 'carbon-components/scss/globals/scss/styles.scss';
import { Content, Header, HeaderName , TextInput , Form , Button} from 'carbon-components-react';

class App extends Component {
  
  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  
  componentDidMount() {
    const {fetchTasks} = this.props;
    fetchTasks();
  }

  handleSubmit(event){
    event.preventDefault();
    const description = event.target.elements.taskDescription.value;
    const task = {
      id: Date.now() +'',
      description: description,
    }    
    if(description){
      this.props.startAddingtask(task);
      event.target.elements.taskDescription.value = '' ;
    }
  }

  render (){ 
   return ( <div className="App">
    <Header aria-label="IBM Platform Name">
      <HeaderName href="#" prefix="TO DO ">
        APP
      </HeaderName>
    </Header>
      <Content>
      <Form
      className="some-class"
      onSubmit={this.handleSubmit}>
      <TextInput
          className="some-class"
          invalid={false}
          invalidText="A valid value is required"
          labelText="Create New Task"
          light={true}
          onChange={function noRefCheck(){}}
          onClick={function noRefCheck(){}}
          id="taskInputField"
          placeholder="write you To do task"
          size='sm'
          type="text" 
          name = 'taskDescription'
          >
          </TextInput>
          <Button
          className="create-task-btn"
          kind="primary"
          type="submit"
          > Create Task</Button>
        </Form>
        <h1> Your current to do list </h1> 
        <TaskList {...this.props}/>
      </Content>
  </div> )
}
}

export default App;
