import React , {Component} from 'react'
import SingleTask from './singleTask'
import '../styles/style.css';

class TaskList extends Component {
    render () {
        return (
            <div className="tasks-list">
            {this.props.tasks.map((task, index) => <SingleTask key={index} task= {task}
             startRemovingtask={this.props.startRemovingtask}  startUpdatingTask ={this.props.startUpdatingTask}
            index = {index}/>) }
           </div>
       );
    }
}

export default TaskList
