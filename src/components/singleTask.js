import React, { Component } from 'react'
import { Accordion, AccordionItem, Button  , TextInput , Form} from 'carbon-components-react';
import '../styles/style.css';

class SingleTask extends Component {

    constructor() {
        super();
        this.handleSubmit = this.handleSubmit.bind(this);
      }

      handleSubmit(event) {
        event.preventDefault();
        if(event.target.elements.edittaskDescription.value) { 
         const description = event.target.elements.edittaskDescription.value;
         const task = {
             id: this.props.task.id,
             description: description,
        }
        this.props.startUpdatingTask(this.props.index,task)
        }
      }

    render() {
        return (
                <Accordion align="end">
                    <AccordionItem
                        onClick={function noRefCheck() { }}
                        onHeadingClick={function noRefCheck() { }}
                        title={this.props.task.description}
                    >
                        <Button
                            className="delete-button"
                            disabled={false}
                            kind="danger"
                            tabIndex={0}
                            onClick = {() => {this.props.startRemovingtask(this.props.index,this.props.task.description)}}
                            type="button"
                        >Delete Item
                        </Button>
                        <Form  
                         className="edit-form" onSubmit={this.handleSubmit}>
                        <TextInput
                            className="some-class"
                            invalid={false}
                            invalidText="A valid value is required"
                            light={true}
                            onChange={function noRefCheck(){}}
                            onClick={function noRefCheck(){}}
                            id="taskInputField"
                            placeholder={this.props.task.description}
                            size='sm'
                            type="text" 
                            name = 'edittaskDescription'
                            labelText = "Edit Task"
                        ></TextInput>
                          <Button
                            disabled={false}
                            kind="secondary"
                            tabIndex={1}
                            type="submit"
                            className="edit-button"
                        >   Edit Item
                        </Button>
                        </Form>
                    </AccordionItem>
                </Accordion>
        );
    }
}

export default SingleTask
