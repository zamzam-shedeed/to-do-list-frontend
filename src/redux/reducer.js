import {combineReducers} from 'redux'

function tasks(state = [] , action) {
    switch(action.type){
    case 'LOAD_TASKS' : return action.tasks
    case 'ADD_TASK' : return [...state,action.tasks] 
    case 'REMOVE_POST': return [...state.slice(0,action.index) , ...state.slice(action.index+1)]
    case "UPDATE_TASK": return [...state.slice(0,action.index) , ...state.slice(action.index+1) , action.task]
    default  : return state;
    }
}

const rootReducer = combineReducers ({tasks})

export default rootReducer
