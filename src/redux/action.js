export function fetchTasks() {
  return dispatch => {
      fetch('http://localhost:8081/todo/App/api/todos')
      .then(res => res.json())
      .then(res => {
          if(res.error) {
              throw(res.error);
          }
          dispatch(loadTasks(res.tasks));
      }).catch(error => {
        console.log('error' , error);
      })
  }
}

export function startAddingtask(task){
  return (dispatch ) => {
     fetch('http://localhost:8081/todo/App/api/todo', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          id: task.id,
          description: task.description
        })
      }).then(response => response.json())
      .then(res => {
        dispatch(addTask(res.tasks));
      });
  }
}


export function startRemovingtask(index,id){
  return (dispatch)=>{
    fetch('http://localhost:8081/todo/App/api/todo/'+id, {
      method: 'DELETE',
      }).then(() => {
      dispatch(removeTask(index));
    });
  }
}

export function startUpdatingTask(index, task){
  return (dispatch)=>{
    fetch('http://localhost:8081/todo/App/api/todo/'+task.id, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        id: task.id,
        description: task.description
      })
    }).then(response => response.json())
    .then(res => {
      dispatch(updateTask(res.updatedTask , index));
    });
  }
}

export function loadTasks(tasks){
    return {
      type : 'LOAD_TASKS',
      tasks
    }
  }

  export function addTask(tasks){
    return {
      type : 'ADD_TASK',
      tasks
    }
  }

  export function removeTask(index){
    return {
      type : 'REMOVE_POST',
      index
    }
  }

  export function updateTask(task , index){
    return {
      type : 'UPDATE_TASK',
      task,
      index
    }
  }