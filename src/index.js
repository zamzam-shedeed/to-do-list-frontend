import React from 'react';
import ReactDOM from 'react-dom';
import Main from './components/Main';
import {createStore , applyMiddleware} from 'redux'
import rootReducer from './redux/reducer'
import {Provider} from 'react-redux'
import thunk from 'redux-thunk'
import './styles/style.css';


const store = createStore(rootReducer,applyMiddleware(thunk));

ReactDOM.render(<Provider store={store}><Main/></Provider>,document.getElementById('root'));
